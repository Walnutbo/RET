import os
import sys
import json
import pickle
import random
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch
from tqdm import tqdm
from torch.autograd import Variable
import matplotlib.pyplot as plt
from sklearn.metrics import f1_score
from sklearn.metrics import roc_curve, auc

def read_split_data(root: str, val_rate: float = 0.01):
    random.seed(0)  # 保证随机结果可复现
    assert os.path.exists(root), "dataset root: {} does not exist.".format(root)

    # 遍历文件夹，一个文件夹对应一个类别
    flower_class = [cla for cla in os.listdir(root) if os.path.isdir(os.path.join(root, cla))]
    # 排序，保证各平台顺序一致
    flower_class.sort()
    # 生成类别名称以及对应的数字索引
    class_indices = dict((k, v) for v, k in enumerate(flower_class))
    json_str = json.dumps(dict((val, key) for key, val in class_indices.items()), indent=4)
    with open('class_indices.json', 'w') as json_file:
        json_file.write(json_str)

    train_images_path = []  # 存储训练集的所有图片路径
    train_images_label = []  # 存储训练集图片对应索引信息
    val_images_path = []  # 存储验证集的所有图片路径
    val_images_label = []  # 存储验证集图片对应索引信息
    every_class_num = []  # 存储每个类别的样本总数
    supported = [".jpg", ".JPG", ".png", ".PNG", ".mat"]  # 支持的文件后缀类型
    # 遍历每个文件夹下的文件
    for cla in flower_class:
        cla_path = os.path.join(root, cla)
        # 遍历获取supported支持的所有文件路径
        images = [os.path.join(root, cla, i) for i in os.listdir(cla_path)
                  if os.path.splitext(i)[-1] in supported]
        # 排序，保证各平台顺序一致
        images.sort()
        # 获取该类别对应的索引
        image_class = class_indices[cla]
        # 记录该类别的样本数量
        every_class_num.append(len(images))
        # 按比例随机采样验证样本
        val_path = random.sample(images, k=int(len(images) * val_rate))

        for img_path in images:
            if img_path in val_path:  # 如果该路径在采样的验证集样本中则存入验证集
                val_images_path.append(img_path)
                val_images_label.append(image_class)
            else:  # 否则存入训练集
                train_images_path.append(img_path)
                train_images_label.append(image_class)

    print("{} images were found in the dataset.".format(sum(every_class_num)))
    print("{} images for training.".format(len(train_images_path)))
    print("{} images for validation.".format(len(val_images_path)))
    '''assert len(train_images_path) > 0, "number of training images must greater than 0."
    assert len(val_images_path) > 0, "number of validation images must greater than 0."'''

    plot_image = False
    if plot_image:
        # 绘制每种类别个数柱状图
        plt.bar(range(len(flower_class)), every_class_num, align='center')
        # 将横坐标0,1,2,3,4替换为相应的类别名称
        plt.xticks(range(len(flower_class)), flower_class)
        # 在柱状图上添加数值标签
        for i, v in enumerate(every_class_num):
            plt.text(x=i, y=v + 5, s=str(v), ha='center')
        # 设置x坐标
        plt.xlabel('image class')
        # 设置y坐标
        plt.ylabel('number of images')
        # 设置柱状图的标题
        plt.title('flower class distribution')
        plt.show()

    return train_images_path, train_images_label, val_images_path, val_images_label


def write_pickle(list_info: list, file_name: str):
    with open(file_name, 'wb') as f:
        pickle.dump(list_info, f)


def read_pickle(file_name: str) -> list:
    with open(file_name, 'rb') as f:
        info_list = pickle.load(f)
        return info_list


def train_one_epoch(model, optimizer, data_loader, device, epoch, y_true, y_score):
    model.train()
    loss_function = torch.nn.CrossEntropyLoss()
    # loss_function = FocalLoss(gamma=2)
    accu_loss = torch.zeros(1).to(device)  # 累计损失
    accu_num = torch.zeros(1).to(device)   # 累计预测正确的样本数
    optimizer.zero_grad()
    sample_num = 0

    data_loader = tqdm(data_loader, file=sys.stdout)
    for step, data in enumerate(data_loader):
        images, labels = data
        sample_num += images.shape[0]
        pred = model(images.to(device))
        pred_classes = torch.max(pred, dim=1)[1]
        print(pred_classes)
        y_true += labels.cpu().tolist()
        y_score += torch.max(nn.Sigmoid()(pred), dim=1)[0].cpu().tolist()

        accu_num += torch.eq(pred_classes, labels.to(device)).sum()
        loss = loss_function(pred, labels.to(device))
        loss.requires_grad_(True)
        loss.backward()

        accu_loss += loss.detach()

        data_loader.desc = "[train epoch {}] loss: {:.4f}, acc: {:.4f}".format(epoch,
                                                                               accu_loss.item() / (step + 1),
                                                                               accu_num.item() / sample_num)
        if not torch.isfinite(loss):
            print('WARNING: non-finite loss, ending training ', loss)
            sys.exit(1)

        optimizer.step()
        optimizer.zero_grad()

    fpr, tpr, thre = roc_curve(y_true, y_score)
    ##计算auc的值，就是roc曲线下的面积
    aucc = auc(fpr, tpr)
    ##画图
    plt.cla()
    plt.plot(fpr, tpr, color='darkred', label='roc area:(%0.2f)' % aucc)
    plt.plot([0, 1], [0, 1], linestyle='--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic')
    plt.legend(loc='lower right')

    dirr = "./image/train_epoch"
    dirr = dirr + str(epoch)
    dirr = dirr + ".png"
    if epoch > 5:
        plt.savefig(dirr)

    return accu_loss.item() / (step + 1), accu_num.item() / sample_num


@torch.no_grad()
def evaluate(model, data_loader, device, epoch, y_true, y_score):
    loss_function = torch.nn.CrossEntropyLoss()
    # loss_function = FocalLoss(gamma=2)
    model.eval()

    accu_num = torch.zeros(1).to(device)   # 累计预测正确的样本数
    accu_loss = torch.zeros(1).to(device)  # 累计损失
    accu_f1 = torch.zeros(1).to(device)
    sample_num = 0

    data_loader = tqdm(data_loader, file=sys.stdout)
    for step, data in enumerate(data_loader):
        images, labels = data
        sample_num += images.shape[0]
        pred = model(images.to(device))

        pred_classes = torch.max(pred, dim=1)[1]

        y_true += labels.cpu().tolist()
        y_score += torch.max(nn.Sigmoid()(pred), dim=1)[0].cpu().tolist()

        accu_f1 += f1_score(labels.cpu(), pred_classes.cpu(), average='macro')
        accu_num += torch.eq(pred_classes, labels.to(device)).sum()

        loss = loss_function(pred, labels.to(device))
        accu_loss += loss

        '''data_loader.desc = "[va-epo {}]  {:.4f}:{:.4f}:{:.4f}".format(epoch,
                                                                      accu_loss.item() / (step + 1),
                                                                      accu_num.item() / sample_num,
                                                                      accu_f1.item() / (step + 1))'''
        data_loader.desc = "[valid epoch {}] loss: {:.4f}, acc: {:.4f}".format(epoch,
                                                                               accu_loss.item() / (step + 1),
                                                                               accu_num.item() / sample_num)


    fpr, tpr, thre = roc_curve(y_true, y_score)
    ##计算auc的值，就是roc曲线下的面积
    aucc = auc(fpr, tpr)
    ##画图
    plt.cla()
    plt.plot(fpr, tpr, color='darkred', label='roc area:(%0.2f)' % aucc)
    plt.plot([0, 1], [0, 1], linestyle='--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic')
    plt.legend(loc='lower right')

    dirr = "./image/value_epoch"
    dirr = dirr + str(epoch)
    dirr = dirr + ".png"
    if epoch > 5:
        plt.savefig(dirr)

    return accu_loss.item() / (step + 1), accu_num.item() / sample_num
    '''

    # 创建一个特征空间范围内的网格
    x_min, x_max = 0, 1  # 这里使用一个示例范围。请根据您的特征数据调整范围。
    y_min, y_max = 0, 1
    h = 0.1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))

    # 转换为 PyTorch tensor，计算网格上每个点的预测概率
    meshgrid_points = torch.tensor(np.c_[xx.ravel(), yy.ravel()]).to(device)
    model.eval()
    with torch.no_grad():
        Z = model(meshgrid_points.to(device))
        Z = torch.softmax(Z, dim=1)[:, 1].detach().cpu().numpy()

    # 调整预测概率数组的形状以匹配网格
    Z = Z.reshape(xx.shape)

    # 绘制热力图
    plt.figure()
    plt.contourf(xx, yy, Z, cmap=plt.cm.RdYlBu, alpha=0.8)
    plt.xlabel('Feature 1')
    plt.ylabel('Feature 2')
    plt.title('Heatmap for Binary Classification at Epoch {epoch}')

    dirr = "./heatmap_image/epoch"
    dirr = dirr + str(epoch)
    dirr = dirr + ".png"
    plt.savefig(dirr)
'''

