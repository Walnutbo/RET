import torch
from torch.utils.data import Dataset
from scipy.io import loadmat
import pickle

class MyDataSet(Dataset):
    """自定义数据集"""

    def __init__(self, images_path: list, images_class: list, transform=None):
        self.images_path = images_path
        self.images_class = images_class
        self.transform = transform

    def __len__(self):
        return len(self.images_path)

    def __getitem__(self, item):
        eeg_data = loadmat(self.images_path[item])
        eeg_data = eeg_data['data']

        eeg_data = eeg_data[:, :, :35]
        label = self.images_class[item]

        if self.transform is not None:
            eeg_data = self.transform(eeg_data)

        '''eeg_data = eeg_data.transpose(0, 2)
        eeg_data = torch.flatten(eeg_data, start_dim=1)'''
        return eeg_data, label

    @staticmethod
    def collate_fn(batch):
        images, labels = tuple(zip(*batch))

        images = torch.stack(images, dim=0)
        labels = torch.as_tensor(labels)
        return images, labels
