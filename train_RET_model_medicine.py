import os
import math
import argparse
import torch
import torch.nn as nn
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms
import torchvision.models as models
from my_dataset import MyDataSet
from RET_model_medicine import RET_model as create_model
from utils import read_split_data, train_one_epoch, evaluate

import random
import numpy as np
from PIL import Image, ImageEnhance, ImageFilter
from skimage.filters import gaussian
from skimage.transform import warp, AffineTransform
from skimage.util import random_noise

class RandomContrast:
    def __init__(self, min_factor=0.7, max_factor=1.3):
        self.min_factor = min_factor
        self.max_factor = max_factor

    def __call__(self, img):
        contrast_factor = random.uniform(self.min_factor, self.max_factor)
        enhanced_img = np.zeros_like(img)
        for channel in range(img.shape[2]):
            img_channel = img[:,:,channel]
            img_pil = Image.fromarray(img_channel.astype(np.uint8))
            enhanced_img_pil = ImageEnhance.Contrast(img_pil).enhance(contrast_factor)
            enhanced_img[:,:,channel] = np.array(enhanced_img_pil)
        return enhanced_img

class RandomSharpness:
    def __init__(self, filter_type='sharpen', min_radius=0.2, max_radius=2.0):
        self.filter_type = filter_type
        self.min_radius = min_radius
        self.max_radius = max_radius

    def __call__(self, img):
        radius = random.uniform(self.min_radius, self.max_radius)
        modified_img = np.zeros_like(img)
        for channel in range(img.shape[2]):
            img_channel = img[:,:,channel]
            img_pil = Image.fromarray(img_channel.astype(np.uint8))
            if self.filter_type == 'sharpen':
                modified_img_pil = img_pil.filter(ImageFilter.SHARPEN)
            elif self.filter_type == 'blur':
                modified_img_pil = img_pil.filter(ImageFilter.GaussianBlur(radius=radius))
            modified_img[:,:,channel] = np.array(modified_img_pil)
        return modified_img


class RandomElasticDeformation:
    def __init__(self, alpha_affine_range=30, alpha_range=None, sigma_range=None):
        self.alpha_affine_range = alpha_affine_range
        self.alpha_range = alpha_range
        self.sigma_range = sigma_range

    def __call__(self, img):
        center_shift = np.array(img.shape[:2]) / 2.0
        random_state = np.random.RandomState(None)
        alpha_affine = random_state.uniform(-self.alpha_affine_range, self.alpha_affine_range)
        corner_shifts = np.array([[1, 1], [-1, 1], [-1, -1], [1, -1]])
        corners = center_shift + alpha_affine * corner_shifts
        pre_corners = center_shift + (center_shift * corner_shifts)
        transform_matrix = AffineTransform()
        transform_matrix.estimate(pre_corners, corners)
        deformed_img = warp(img, transform_matrix.inverse)

        if self.alpha_range is not None and self.sigma_range is not None:
            alpha = random.uniform(self.alpha_range[0], self.alpha_range[1])
            sigma = random.uniform(self.sigma_range[0], self.sigma_range[1])
            noise = random_noise(random_state.randn(*img.shape), mode='gaussian', seed=None, clip=True, var=sigma ** 2)
            transformed_noise = warp(noise, transform_matrix.inverse)
            deformed_img += alpha * transformed_noise

        return (255 * deformed_img).astype(np.uint8)


def main(args):
    device = torch.device(args.device if torch.cuda.is_available() else "cpu")
    if os.path.exists("./weights") is False:
        os.makedirs("./weights")

    tb_writer = SummaryWriter()

    train_images_path, train_images_label, val_images_path, val_images_label = read_split_data(args.data_path)

    data_transform = {
        "train": transforms.Compose([
            RandomContrast(),
            RandomSharpness(),
            RandomElasticDeformation(),
            transforms.ToTensor(),
        ]),
        "val": transforms.Compose([
            transforms.ToTensor(),
        ])
    }
    # 实例化训练数据集
    train_dataset = MyDataSet(images_path=train_images_path,
                              images_class=train_images_label,
                              transform=data_transform["train"])

    # 实例化验证数据集
    val_dataset = MyDataSet(images_path=val_images_path,
                            images_class=val_images_label,
                            transform=data_transform["val"])

    batch_size = args.batch_size
    nw = min([os.cpu_count(), batch_size if batch_size > 1 else 0, 8])  # number of workers
    print('Using {} dataloader workers every process'.format(nw))
    train_loader = torch.utils.data.DataLoader(train_dataset,
                                               batch_size=batch_size,
                                               shuffle=True,
                                               pin_memory=True,
                                               num_workers=nw,
                                               collate_fn=train_dataset.collate_fn)

    val_loader = torch.utils.data.DataLoader(val_dataset,
                                             batch_size=batch_size,
                                             shuffle=False,
                                             pin_memory=True,
                                             num_workers=nw,
                                             collate_fn=val_dataset.collate_fn)

    model = create_model(num_classes=args.num_classes).to(device)

    if args.weights != "":
        weights_dict = torch.load(args.weights, map_location=device)
        print(model.load_state_dict(weights_dict, strict=False))

    # optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9, weight_decay=5E-5)
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=5E-5)
    lf = lambda x: ((1 + math.cos(x * math.pi / args.epochs)) / 2) * (1 - args.lrf) + args.lrf  # cosine
    scheduler = lr_scheduler.LambdaLR(optimizer, lr_lambda=lf)

    for epoch in range(args.epochs):
        y_true, y_score = [], []
        # train
        train_loss, train_acc = train_one_epoch(model=model,
                                                optimizer=optimizer,
                                                data_loader=train_loader,
                                                device=device,
                                                epoch=epoch,
                                                y_true=y_true,
                                                y_score=y_score)

        scheduler.step()

        # validate
        val_loss, val_acc = evaluate(model=model,
                                     data_loader=val_loader,
                                     device=device,
                                     epoch=epoch,
                                     y_true=y_true,
                                     y_score=y_score)

        tags = ["train_loss", "train_acc", "val_loss", "val_acc", "learning_rate"]
        tb_writer.add_scalar(tags[0], train_loss, epoch)
        tb_writer.add_scalar(tags[1], train_acc, epoch)
        tb_writer.add_scalar(tags[2], val_loss, epoch)
        tb_writer.add_scalar(tags[3], val_acc, epoch)
        tb_writer.add_scalar(tags[4], optimizer.param_groups[0]["lr"], epoch)

        torch.save(model.state_dict(), "./weights/model_RET_medicine-{}.pth".format(epoch))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--num_classes', type=int, default=2)
    parser.add_argument('--epochs', type=int, default=80)
    parser.add_argument('--batch-size', type=int, default=1)
    parser.add_argument('--lr', type=float, default=0.01)
    parser.add_argument('--lrf', type=float, default=0.0001)

    # 数据集所在根目录
    # https://storage.googleapis.com/download.tensorflow.org/example_images/flower_photos.tgz
    parser.add_argument('--data-path', type=str,
                        default="./EEG_data_mat")
    parser.add_argument('--model-name', default='', help='create model name')

    # 预训练权重路径，如果不想载入就设置为空字符
    parser.add_argument('--weights', type=str, default='',
                        help='initial weights path')
    # parser.add_argument('--weights', type=str, default='vit_base_patch16_224_in21k.pth',
    #                     help='initial weights path')
    parser.add_argument('--device', default='cuda', help='device id (i.e. 0 or 0,1 or cpu)')

    opt = parser.parse_args()

    main(opt)
